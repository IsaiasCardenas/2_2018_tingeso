-- DROP IF EXIST market;

-- CREATE DATABASE market;

-- USE market;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(256) NOT NULL,
  `price` decimal(13,4) NOT NULL,
  `expiration_date` date NOT NULL,
  `category` enum('IMPORTADO','NACIONAL') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Seed
--
INSERT INTO products (category, code, expiration_date, name, price) VALUES
('NACIONAL', '359', '2019-03-03', 'Makayla Networked regional intranet', 8365),
('IMPORTADO', '858', '2019-02-11', 'Jewel Reduced intangible concept', 3415),
('IMPORTADO', '540', '2019-09-30', 'Janis Advanced secondary paradigm', 3046),
('NACIONAL', '798', '2019-10-17', 'Benton Inverse non-volatile middleware', 5970),
('NACIONAL', '683', '2020-04-10', 'Creola Total eco-centric instructionset', 4964),
('IMPORTADO', '645', '2021-07-14', 'Ernest User-centric optimal synergy', 5441),
('NACIONAL', '441', '2021-10-16', 'Johnnie Business-focused zerodefect groupware', 6205),
('IMPORTADO', '994', '2019-03-07', 'Chelsie Devolved asynchronous hierarchy', 4958),
('IMPORTADO', '993', '2019-04-19', 'Danial Devolved actuating function', 9789),
('NACIONAL', '402', '2020-09-08', 'Theodore Innovative object-oriented encryption', 8204),
('NACIONAL', '598', '2020-07-22', 'Jerrell Compatible 4thgeneration time-frame', 297),
('IMPORTADO', '567', '2019-08-27', 'Johathan Right-sized stable time-frame', 8586),
('IMPORTADO', '156', '2019-10-29', 'Jennie Configurable stable knowledgeuser', 1650),
('IMPORTADO', '426', '2020-03-17', 'Cullen Fundamental methodical artificialintelligence', 726),
('IMPORTADO', '900', '2020-01-22', 'Danny Cross-platform web-enabled customerloyalty', 3394),
('IMPORTADO', '484', '2020-06-03', 'Myriam Function-based high-level application', 1979),
('NACIONAL', '493', '2020-03-04', 'Leann Public-key nextgeneration flexibility', 2969),
('NACIONAL', '848', '2021-09-09', 'Rossie Automated system-worthy circuit', 2726),
('NACIONAL', '179', '2018-11-19', 'Gwendolyn Function-based fault-tolerant focusgroup', 6131),
('NACIONAL', '247', '2021-10-05', 'Daphney Future-proofed foreground leverage', 3299),
('IMPORTADO', '772', '2021-09-08', 'Zakary Focused interactive artificialintelligence', 5245),
('IMPORTADO', '240', '2021-04-28', 'Helen Balanced didactic structure', 3896),
('IMPORTADO', '932', '2021-01-24', 'Willie Proactive high-level hardware', 904),
('NACIONAL', '657', '2019-12-05', 'Dane Diverse asynchronous customerloyalty', 9899),
('NACIONAL', '935', '2020-08-31', 'Ola Re-contextualized exuding time-frame', 8604),
('NACIONAL', '263', '2019-03-09', 'Nyah Universal impactful knowledgebase', 4242),
('IMPORTADO', '808', '2019-05-05', 'Gaston Optimized multi-tasking opensystem', 212),
('IMPORTADO', '838', '2020-12-05', 'Karson Function-based empowering alliance', 7815),
('IMPORTADO', '645', '2021-10-04', 'Jada Optional web-enabled blockchain', 1298),
('IMPORTADO', '323', '2021-01-02', 'Charity Optional optimizing moderator', 5985),
('NACIONAL', '342', '2018-12-12', 'Frederique Ameliorated eco-centric concept', 6401),
('NACIONAL', '994', '2019-09-04', 'Pearline Robust hybrid artificialintelligence', 4079),
('NACIONAL', '406', '2020-07-16', 'Ola Fully-configurable object-oriented hub', 8938),
('NACIONAL', '612', '2020-12-15', 'Kali Assimilated even-keeled migration', 3457),
('NACIONAL', '434', '2021-11-01', 'Darwin Customizable zeroadministration parallelism', 2766),
('IMPORTADO', '452', '2021-09-22', 'Lessie Optional fresh-thinking implementation', 77),
('IMPORTADO', '758', '2021-10-06', 'Assunta Programmable dedicated orchestration', 23),
('IMPORTADO', '886', '2021-06-23', 'Carrie Sharable 3rdgeneration encryption', 1096),
('IMPORTADO', '794', '2020-07-27', 'Alden Down-sized 24hour circuit', 139),
('NACIONAL', '343', '2020-06-20', 'Sabina Distributed transitional hub', 9265),
('NACIONAL', '949', '2019-08-02', 'Lilla Cloned solution-oriented application', 6019),
('NACIONAL', '680', '2021-04-19', 'Lynn Front-line responsive matrices', 4673),
('NACIONAL', '749', '2019-09-01', 'Jalen Object-based non-volatile task-force', 9494),
('NACIONAL', '136', '2019-06-02', 'Brandy Operative 3rdgeneration success', 4561),
('IMPORTADO', '819', '2021-03-04', 'Blaze Switchable 24/7 middleware', 4088),
('IMPORTADO', '787', '2021-07-05', 'Wayne Reduced incremental frame', 2571),
('NACIONAL', '511', '2020-10-14', 'Dessie Cloned fault-tolerant migration', 3222),
('IMPORTADO', '292', '2019-12-22', 'Mose Horizontal human-resource concept', 6596),
('IMPORTADO', '514', '2019-02-19', 'Selina De-engineered maximized success', 9026),
('IMPORTADO', '554', '2019-02-06', 'Adelbert Reverse-engineered solution-oriented openarchitecture', 9593);