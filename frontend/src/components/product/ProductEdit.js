import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import moment from 'moment';
import axios from 'axios';
import numeral from 'numeral';

const API = 'http://159.65.253.247:8081/backend/';

class ProductEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      error: null,
      product: {
        id: this.props.match.params.id,
        code: "",
        name: "",
        expirationDate: "",
        category: "",
        price: "",
      },
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setState({ isLoading: true });

    document.addEventListener('DOMContentLoaded', function() {
      var elems = document.querySelectorAll('select');
      window.M.FormSelect.init(elems, {});
    });

    window.$(document).ready(function() {
      window.$('input[data-length]').characterCounter();
    });

    axios.get(API + 'products/' + this.state.product.id)
      .then(result => {
        result.data.expirationDate = moment(result.data.expirationDate, 'YYYY-MM-DD').format('DD-MM-YYYY');
        this.setState({
          product: result.data,
          isLoading: false
        });
      })
      .catch(error => this.setState({
        error,
        isLoading: false
      }));
  }

  validateSubmit(event) {
    const form = event.target;
    const product = this.state.product;

    const properties = [
      'code',
      'name',
      'expirationDate',
      'category',
      'price',
    ];

    var outcome = true;
    
    try {
      // validar que existan las propiedades
      for (var i = 0; i < properties.length; i++) {
        form.elements[properties[i]].classList = "validate";
        if ( ! product.hasOwnProperty(properties[i]) || 
              (typeof product[properties[i]] === "string" && product[properties[i]].trim().length === 0)
            ) {
          form.elements[properties[i]].classList += " invalid";
          outcome = false;
        }
      }

      if ( ! outcome) {
        window.M.toast({html: 'Faltan campos obligatorios'});
      }

      // validar formato 'code' max-length=10
      if (product.code.length > 10) {
        form.elements['code'].classList += " invalid";
        window.M.toast({html: 'Largo del nombre no puede ser mayor a 10 caracteres'});
        outcome = false;
      }

      // validar formato de 'name' max-length=256
      if (product.name.length > 256) {
        form.elements['name'].classList += " invalid";
        window.M.toast({html: 'Largo del nombre no puede ser mayor a 256 caracteres'});
        outcome = false;
      }

      // validar formato de 'expirationDate'
      if ( ! moment(product.expirationDate, 'DD-MM-YYYY', true).isValid()) {
        form.elements['expirationDate'].classList += " invalid";
        window.M.toast({html: 'Formato de fecha inválido'});
        outcome = false;
      }

      // validar price {0-9}.{0-4}
      var price = numeral(product.price).format('0.00');
      form.elements['price'].value = price;
      var regex  = /^\d{0,9}\.\d{0,2}/;
      if ( ! regex.test(price)) {
        form.elements['price'].classList += " invalid";
        window.M.toast({html: 'Formato de precio no es válido'});
        outcome = false;
      }

    } catch (err) {
      console.log(err);
      window.M.toast({html: 'Whoops, ha ocurrido un error'});
    }

    event.preventDefault();
    event.stopPropagation();
    return outcome;
  }

  handleSubmit(event) {
    this.setState({ isLoading: true });

    if ( ! this.validateSubmit(event)) {
      event.preventDefault();
      return;
    }

    let product = {...this.state.product};
    product.expirationDate = moment(product.expirationDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
    product.price = numeral(product.price).format('0[.]0000')

    axios.put(API + 'products/' + this.state.product.id, product)
      .then(result => {
        this.setState({
          isLoading: false,
        });

        this.props.history.push('/');
        window.M.toast({html: 'Producto Actualizado'});
      })
      .catch(error => {
        this.setState({
          error,
          isLoading: false
        });

        window.M.toast({html: 'Error al actualizar el producto'});
      });

    event.preventDefault();
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      product: {
        ...this.state.product,
        [name]: value
      }
    });
  }

  render() {
    return (
      <div>
        <h3>Editar Producto</h3>

        <form onSubmit={this.handleSubmit}>
          <div className="input-field">
            <label className="active">Código</label>
            <input type="text" name="code" placeholder="Código" defaultValue={ this.state.product.code } onChange={this.handleChange }  maxLength="10" data-length="10" />
          </div>

          <div className="input-field">
            <label className="active">Nombre</label>
            <input type="text" name="name" placeholder="Nombre" defaultValue={ this.state.product.name }  onChange={this.handleChange } maxLength="256" data-length="256"/> 
          </div>
          
          <div className="input-field">
            <label className="active">Fecha de vencimiento</label>
            <input type="text" name="expirationDate" placeholder="Fecha de vencimiento" defaultValue={ this.state.product.expirationDate }  onChange={this.handleChange } /> 
            <span className="helper-text">Formato: dd-mm-yyyy, Ej: 01-01-2018</span>
          </div>
                    
          <div className="input-field">
            <label className="active">Precio</label>
            <input type="text" name="price" placeholder="Precio" defaultValue={ this.state.product.price }  onChange={this.handleChange } /> 
            <span className="helper-text">Ej: 123456789.0000</span>
          </div>

          <div className="input-field">
            <select name="category" onChange={this.handleChange} defaultValue={ this.state.product.category }>
              <option value="NACIONAL">NACIONAL</option>
              <option value="IMPORTADO">IMPORTADO</option>
            </select>
            <label>Categoría</label>
          </div>
          
          <div className="input-field">
            <Link className="btn waves-effect waves-light" to={'/products/'+this.state.product.id}>
              <i className="material-icons left">arrow_back</i> Volver
            </Link>

            <button type="submit" className="btn blue right">
              <i className="material-icons left">save</i> Guardar
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default ProductEdit;
