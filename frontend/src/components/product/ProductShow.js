import React, { Component } from 'react';
import { Link } from "react-router-dom";

import moment from 'moment';
import axios from 'axios';

const API = 'http://159.65.253.247:8081/backend/';

class ProductShow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: {
        id: this.props.match.params.id,
        code: "",
        name: "",
        expirationDate: "",
        category: "",
        price: "",
      },
      isLoading: false,
      error: null,
    };


  }

  componentDidMount() {
    this.setState({ isLoading: true });

    axios.get(API + 'products/' + this.state.product.id)
      .then(result => {
        result.data.expirationDate = moment(result.data.expirationDate, 'YYYY-MM-DD').format('DD-MM-YYYY');
        this.setState({
          product: result.data,
          isLoading: false
        });
      })
      .catch(error => this.setState({
        error,
        isLoading: false
      }));
  }

  render() {
    return (
      <div>
        <h3>Detalle Producto</h3>

        <div className="form-group">
          <label>Código</label>
          <div>{ this.state.product.code }</div>
        </div>
        <div className="form-group">
          <label>Nombre</label>
          <div>{ this.state.product.name }</div>
        </div>
        <div className="form-group">
          <label>Fecha de vencimiento</label>
          <div>{ this.state.product.expirationDate }</div>
        </div>
        <div className="form-group">
          <label>Categoría</label>
          <div>{ this.state.product.category }</div>
        </div>
        <div className="form-group">
          <label>Precio</label>
          <div>{ this.state.product.price }</div>
        </div>

        <br/>

        <div className="input-field">
          <Link className="btn waves-effect waves-light" to='/'>
            <i className="material-icons left">arrow_back</i> Volver
          </Link>
        
          <Link className="btn right waves-effect waves-light blue" to={ '/products/'+this.state.product.id+'/edit' }>
            <i className="material-icons left">edit</i>
            Editar
          </Link>
        </div>
      </div>
    );
  }
}

export default ProductShow;
