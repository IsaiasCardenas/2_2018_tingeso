import React, { Component } from 'react';
import { Link } from "react-router-dom";

import axios from 'axios';

const API = 'http://159.65.253.247:8081/backend/';

class ProductIndex extends Component {
	constructor(props) {
	  super(props);

	  this.state = {
	    products: [],
	    isLoading: false,
	    error: null,
	  };

	  this.delete = this.delete.bind(this);
	}

	componentDidMount() {
		this.getProducts();
	}

	getProducts() {
	  this.setState({ isLoading: true });

	  axios.get(API + 'products/')
	    .then(result => this.setState({
	      products: result.data,
	      isLoading: false
	    }))
	    .catch(error => this.setState({
	      error,
	      isLoading: false
	    }));
	}

	delete(event) {
		if (window.confirm('¿Esta seguro que desea eliminar el producto?')) {
			const id = event.target.id.value;
			
			this.setState({ isLoading: true });

			axios.delete(API + 'products/' + id)
			  .then(result => {
			  	
			  	this.setState({
				    isLoading: false
				});

			  	this.getProducts();

				window.M.toast({html: 'Producto Eliminado'});
			  })
			  .catch(error => {
		        this.setState({
		          error,
		          isLoading: false
		        });

		        window.M.toast({html: 'Error al eliminar el producto'});
		      });
		}
		
		event.preventDefault();
	}

	render() {
		return (
			<div>
				<h3>Productos</h3>

			    <table>
			    	<thead>
			    		<tr>
			    			<th>Code</th>
			    			<th>Name</th>
			    			<th>Price</th>
			    			<th></th>
			    			<th></th>
			    		</tr>
			    	</thead>
			    	<tbody>
				      {this.state.products.map(product =>
			    		<tr key={product.id}>
			    			<td>{product.code}</td>
			    			<td>{product.name}</td>
			    			<td>{product.price}</td>
			    			<td>
					        	<Link to={'/products/'+product.id} className="btn waves-effect waves-light blue">
					        		<i className="material-icons">launch</i>
					        	</Link>
			    			</td>
			    			<td>
			    				<form onSubmit={this.delete}>
			    					<input type="hidden" name="id" value={product.id}/>
				    				<button type="submit" className="btn waves-effect waves-light red">
				    					<i className="material-icons">delete_forever</i>
				    				</button>
			    				</form>
			    			</td>
				      </tr>
				      )}
			      </tbody>
			    </table>
			</div>
		);
	}
}

export default ProductIndex;
