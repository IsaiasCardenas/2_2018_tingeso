import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import ProductIndex from './components/product/ProductIndex';
import ProductCreate from './components/product/ProductCreate';
import ProductShow from './components/product/ProductShow';
import ProductEdit from './components/product/ProductEdit';

import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <nav>
            <div className="nav-wrapper">
              <Link to='/' className="brand-logo">ProductoTeca</Link>
              <ul className="right hide-on-med-and-down">
                <li>
                  <Link to='/'>
                    <i className="material-icons left">list</i>Listado
                  </Link>
                </li>
                <li>
                  <Link to='/products/create'>
                    <i className="material-icons left">add</i>Agregar Item
                  </Link>
                </li>
              </ul>
            </div>
          </nav>
          
          <div className="container">
            <Switch>
              <Route exact path="/" component={ProductIndex} />
              <Route exact path="/products/create" component={ProductCreate} />
              <Route exact path="/products/:id" component={ProductShow} />
              <Route exact path="/products/:id/edit" component={ProductEdit} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
