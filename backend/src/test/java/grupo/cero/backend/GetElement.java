package grupo.cero.backend;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GetElement {
	
WebDriver driver = new FirefoxDriver();
	
    @BeforeTest
    public void setUp(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://159.65.253.247/");
    }
    @Test
    public void findElementbyXpath(){
        driver.findElement(By.xpath("/html/body/div/div/div/div/table/tbody/tr[1]/td[4]/a")).click();
    }
    @AfterTest
    public void doThis(){
        driver.quit();
    }

}
