package grupo.cero.backend;

import org.junit.jupiter.api.Test;
import grupo.cero.backend.model.Product;
import static org.junit.jupiter.api.Assertions.*;
import grupo.cero.backend.repository.ProductRepository;


class ProductControllerTest {

    private ProductRepository productRepository;

    @Test
    void testGetFindByName() {
        Product product = productRepository.findByName("Janis Advanced secondary paradigm");
        assertNotNull(product);
    }

    @Test
    void testGetProducts() {
        assertNotNull(productRepository.findAll());
    }
}
