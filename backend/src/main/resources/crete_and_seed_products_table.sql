-- DROP IF EXIST market;

-- CREATE DATABASE market;

-- USE market;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` tinyint(3) NOT NULL,
  `name` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `expiration_date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `category` varchar(9) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Seed
--
INSERT INTO products (id, category, code, expiration_date, name, price) VALUES
(28, 'Nacional', '359', '2019-03-03 11:03:01', 'Makayla Networked regional intranet', 8365),
(44, 'Importado', '858', '2019-02-11 22:15:41', 'Jewel Reduced intangible concept', 3415),
(8, 'Importado', '540', '2019-09-30 20:18:14', 'Janis Advanced secondary paradigm', 3046),
(43, 'Nacional', '798', '2019-10-17 04:46:06', 'Benton Inverse non-volatile middleware', 5970),
(27, 'Nacional', '683', '2020-04-10 06:48:36', 'Creola Total eco-centric instructionset', 4964),
(37, 'Importado', '645', '2021-07-14 21:35:56', 'Ernest User-centric optimal synergy', 5441),
(32, 'Nacional', '441', '2021-10-16 09:03:56', 'Johnnie Business-focused zerodefect groupware', 6205),
(26, 'Importado', '994', '2019-03-07 23:06:18', 'Chelsie Devolved asynchronous hierarchy', 4958),
(22, 'Importado', '993', '2019-04-19 21:38:14', 'Danial Devolved actuating function', 9789),
(9, 'Nacional', '402', '2020-09-08 14:36:53', 'Theodore Innovative object-oriented encryption', 8204),
(48, 'Nacional', '598', '2020-07-22 14:48:43', 'Jerrell Compatible 4thgeneration time-frame', 297),
(34, 'Importado', '567', '2019-08-27 05:25:02', 'Johathan Right-sized stable time-frame', 8586),
(31, 'Importado', '156', '2019-10-29 17:58:20', 'Jennie Configurable stable knowledgeuser', 1650),
(5, 'Importado', '426', '2020-03-17 09:11:33', 'Cullen Fundamental methodical artificialintelligence', 726),
(45, 'Importado', '900', '2020-01-22 10:38:49', 'Danny Cross-platform web-enabled customerloyalty', 3394),
(13, 'Importado', '484', '2020-06-03 16:44:18', 'Myriam Function-based high-level application', 1979),
(30, 'Nacional', '493', '2020-03-04 09:08:57', 'Leann Public-key nextgeneration flexibility', 2969),
(21, 'Nacional', '848', '2021-09-09 10:23:20', 'Rossie Automated system-worthy circuit', 2726),
(50, 'Nacional', '179', '2018-11-19 16:00:54', 'Gwendolyn Function-based fault-tolerant focusgroup', 6131),
(6, 'Nacional', '247', '2021-10-05 04:24:36', 'Daphney Future-proofed foreground leverage', 3299);

INSERT INTO products (id, category, code, expiration_date, name, price) VALUES
(36, 'Importado', '772', '2021-09-08 18:40:24', 'Zakary Focused interactive artificialintelligence', 5245),
(47, 'Importado', '240', '2021-04-28 19:02:01', 'Helen Balanced didactic structure', 3896),
(14, 'Importado', '932', '2021-01-24 19:25:17', 'Willie Proactive high-level hardware', 904),
(35, 'Nacional', '657', '2019-12-05 08:21:35', 'Dane Diverse asynchronous customerloyalty', 9899),
(15, 'Nacional', '935', '2020-08-31 00:15:14', 'Ola Re-contextualized exuding time-frame', 8604),
(2, 'Nacional', '263', '2019-03-09 08:53:13', 'Nyah Universal impactful knowledgebase', 4242),
(11, 'Importado', '808', '2019-05-05 12:03:20', 'Gaston Optimized multi-tasking opensystem', 212),
(10, 'Importado', '838', '2020-12-05 23:23:25', 'Karson Function-based empowering alliance', 7815),
(25, 'Importado', '645', '2021-10-04 21:00:37', 'Jada Optional web-enabled blockchain', 1298),
(7, 'Importado', '323', '2021-01-02 20:11:11', 'Charity Optional optimizing moderator', 5985),
(40, 'Nacional', '342', '2018-12-12 12:07:00', 'Frederique Ameliorated eco-centric concept', 6401),
(20, 'Nacional', '994', '2019-09-04 05:57:54', 'Pearline Robust hybrid artificialintelligence', 4079),
(3, 'Nacional', '406', '2020-07-16 22:57:32', 'Ola Fully-configurable object-oriented hub', 8938),
(29, 'Nacional', '612', '2020-12-15 18:31:36', 'Kali Assimilated even-keeled migration', 3457),
(24, 'Nacional', '434', '2021-11-01 11:27:26', 'Darwin Customizable zeroadministration parallelism', 2766),
(19, 'Importado', '452', '2021-09-22 23:39:47', 'Lessie Optional fresh-thinking implementation', 77),
(17, 'Importado', '758', '2021-10-06 05:52:12', 'Assunta Programmable dedicated orchestration', 23),
(42, 'Importado', '886', '2021-06-23 22:44:04', 'Carrie Sharable 3rdgeneration encryption', 1096),
(39, 'Importado', '794', '2020-07-27 06:22:59', 'Alden Down-sized 24hour circuit', 139),
(23, 'Nacional', '343', '2020-06-20 04:01:38', 'Sabina Distributed transitional hub', 9265);

INSERT INTO products (id, category, code, expiration_date, name, price) VALUES
(38, 'Nacional', '949', '2019-08-02 06:07:40', 'Lilla Cloned solution-oriented application', 6019),
(18, 'Nacional', '680', '2021-04-19 23:05:34', 'Lynn Front-line responsive matrices', 4673),
(16, 'Nacional', '749', '2019-09-01 04:26:23', 'Jalen Object-based non-volatile task-force', 9494),
(33, 'Nacional', '136', '2019-06-02 20:00:58', 'Brandy Operative 3rdgeneration success', 4561),
(41, 'Importado', '819', '2021-03-04 01:15:20', 'Blaze Switchable 24/7 middleware', 4088),
(49, 'Importado', '787', '2021-07-05 21:40:21', 'Wayne Reduced incremental frame', 2571),
(1, 'Nacional', '511', '2020-10-14 11:51:10', 'Dessie Cloned fault-tolerant migration', 3222),
(4, 'Importado', '292', '2019-12-22 17:02:02', 'Mose Horizontal human-resource concept', 6596),
(46, 'Importado', '514', '2019-02-19 22:14:45', 'Selina De-engineered maximized success', 9026),
(12, 'Importado', '554', '2019-02-06 21:09:45', 'Adelbert Reverse-engineered solution-oriented openarchitecture', 9593);