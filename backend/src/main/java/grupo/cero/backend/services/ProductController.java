package grupo.cero.backend.services;

import java.util.List;
import grupo.cero.backend.model.Product;
import org.springframework.http.HttpStatus;
import grupo.cero.backend.repository.ProductRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

// evaluacion martes 13 de noviembre 3:48 pm

@RestController
@CrossOrigin(origins = "http://159.65.253.247:80")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public List<Product> getProducts()
    {
        return productRepository.findAll();
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET, produces = "application/json")
    public Product getProductById(@PathVariable long id) {
        return productRepository.findById(id).get();
    }

    @RequestMapping(value="/products", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Product createProduct(@RequestBody Product product)
    {
        return productRepository.save(product);
    }

//probando aplicacion martes 13-11-2018
    @RequestMapping(value="/products/{id}", method = RequestMethod.PUT)
    public Product updateProduct(@RequestBody Product product, @PathVariable long id) {

        Product p = productRepository.findById(id).get();

        p.setName(product.getName());
        p.setPrice(product.getPrice());
        p.setCategory(product.getCategory());
        p.setCode(product.getCode());
        p.setExpirationDate(product.getExpirationDate());

        productRepository.save(p);
        return p;
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
    public void deleteProduct(@PathVariable long id) {
        productRepository.deleteById(id);
    }
}
