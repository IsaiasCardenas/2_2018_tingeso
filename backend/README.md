# Projecto Base para prueba de integración continua
Projecto para comprobación del funcionamiento de la plataforma de integración continua, para el laboratorio de Tingeso / Mingeso

## Para ejecutar Base de Datos
Para ejecutar la base de datos usando Docker:
```bash
docker run -p 3306:3306 --name myDataBase -e MYSQL_ROOT_PASSWORD=myPass -d mysql
```
Se levantará un contenedor de Docker llamado 'myDataBase' con la última versión de mysql, las credenciales de accesso serán:
**user:root pass:myPass**
Luego debe ingresar al contenedor para crear la base de datos ejecutando el siguiente comando:
```bash
docker exec -it myDataBase bash
```
Donde 'MyDataBase' es el nombre del contenedor, entrará en la consola del contenedor. Luego debe ingresar a la shell de mysql
```bash
mysql -u root -p
```
Ingresando 'myPass' tendrá acceso a la shell de mysql, debe crear la base de datos:

```mysql
CREATE DATABASE dbName;
USE dbName;
```
Finalmente debe realizar la conexión a la base de datos llamada 'dbName'.


## Para iniciar Backend:
Se deben instalar las dependencias y compilar archivos de java:
```bash
mvn spring-boot:run
```
O bien ejectuar desde algun IDE (IntelliJ, Eclipse)


## IP Servidores:
* Servidor SonarQube: 159.65.253.247
* Servidor Jenkins: 206.81.10.40
